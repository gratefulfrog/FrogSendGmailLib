#ifndef ARDUINO_SECRETS_H
#define ARDUINO_SECRETS_H

// either set the defines here or include a file from elsewhere that sets them

#define SECRET_WIFI_SSID                ("My home Wifi network")  // WiFi network name i.e. SSID
#define SECRET_WIFI_PASS                ("My home wifi password") // WiFi password

#define SECRET_SEND_NAME                ("Somebody SomeLastName")  // sender's gmail name                                       
#define SECRET_SEND_ACCOUNT             ("somebody@gmail.com")     // sender's gmail account
#define SECRET_SEND_ACCOUNT_PASSWORD    ("wxyzabcdlmnorstu")       // sender's gmail app aspecific password
#define SECRET_DOMAIN                   ("domain-abc.org")         // sender's domain

#endif
