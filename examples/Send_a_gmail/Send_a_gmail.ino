/*
This example creates an SSLclient object that connects and transfers
data using always SSL.

I then use it to connect to smpt.gmail.com and send an email.

SMTP command reference:  https://www.samlogic.net/articles/smtp-commands-reference.htm

Things to note:
* gmail sending account: must have 2 factor identification enabled and an app specific password created
*                        for this usage
* lots of things are tricky here!
*/
#include <FrogSendGmailLib.h>
#include "arduino_secrets.h"

char *toAddress   = "someone@somewhere.com",
     *toName      = "Somone Somwhere",            // optional
     *subjectText = "This is the Subject", 
     *msgTest     = "This is the email text.";

void configStartup(){
  Serial.begin(115200);
  while(!Serial);
  Serial.println("Enter a character to begin...");
  while (!Serial.available());
  Serial.read();
}

void setup(){
  configStartup();
  
  FrogSendGmail *fsg = new FrogSendGmail( SECRET_WIFI_SSID,
                                          SECRET_WIFI_PASS,
                                          SECRET_DOMAIN,
                                          SECRET_SEND_ACCOUNT,
                                          SECRET_SEND_ACCOUNT_PASSWORD,
                                          SECRET_SEND_NAME);

  int err0 = fsg->connectWifiAndSendSMTPGmail(subjectText, msgTest,toAddress, toName);
  Serial.println(String(FrogSendGmail::errorMessageVec[err0]));
}

void loop() {}
