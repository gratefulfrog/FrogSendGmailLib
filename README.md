# FrogSendGmailLib

Using all that I have learned about smtp and so on, this library should allow one to send simple gmail messages from Arduino boards that use the WifiNina wifi library.
For instructions on setting up WifiNina, see this repo: https://gitlab.com/gratefulfrog/arduino-wifinina-gmail-smtp