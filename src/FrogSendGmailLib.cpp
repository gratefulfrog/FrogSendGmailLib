#include "FrogSendGmailLib.h"


const sendFuncPtr FrogSendGmail::_sendFuncVec[_nbSendFuncs] = {
                                  &FrogSendGmail::_connectWifi,
                                  &FrogSendGmail::_connectServer,
                                  &FrogSendGmail::_send_EHLO,
                                  &FrogSendGmail::_send_AUTH_LOGIN,
                                  &FrogSendGmail::_send_GMail_User_Account,
                                  &FrogSendGmail::_send_Gmail_Password,
                                  &FrogSendGmail::_send_From,
                                  &FrogSendGmail::_send_To,
                                  &FrogSendGmail::_send_DATA,
                                  &FrogSendGmail::_send_Headers_And_Body,
                                  &FrogSendGmail::_send_QUIT};

const String FrogSendGmail::errorMessageVec[] = {
                                  "Message Sent - No Error!",                             //  0
                                  "Failed to Connect to GMail SMTP server",               //  1
                                  "No Reply to Connect",                                  //  2
                                  "No Reply to EHLO",                                     //  3
                                  "No Reply to AUTH LOGIN",                               //  4
                                  "No Reply to Sending GMail User Account",               //  5
                                  "No Reply to Sending Gmail Password",                   //  6
                                  "No Reply to Sending From gmail user mail address",     //  7
                                  "No Reply to Sending To mail address",                  //  8
                                  "No Reply to Sending DATA",                             //  9
                                  "No Reply to Sending Headers and Body",                 // 10
                                  "No Reply to Sending QUIT",                             // 11
                                  "Failed to connect to WIFI"                             // 12
                                  };

FrogSendGmail::FrogSendGmail(const char *wifiSSID,
			     const char *wifiPasword,
			     const char *senderDomain,
			     const char *senderGmailAddress,
			     const char *senderGmailAppPAssword,
			     const char *senderName):
  _ssid_wifi(wifiSSID),
  _pass_wifi(wifiPasword),
  _sendDomain(senderDomain),
  _gAcc(senderGmailAddress),
  _gPass(senderGmailAppPAssword),
  _gName(senderName),
  _port(GMAIL_SERVER_PORT),
  _server(GMAIL_SMTP_SERVER){
    int encodedLength = Base64.encodedLength(_gAcc.length());
    
    _encodedAccount = new char[encodedLength+1];
    _encode64(_gAcc, _encodedAccount);
    _encodedAccount[encodedLength] = '\0';
    
    encodedLength = Base64.encodedLength(_gPass.length());
    _encodedPass = new char[encodedLength+1];
    _encode64(_gPass, _encodedPass);
    _encodedPass[encodedLength] = '\0';
}

int FrogSendGmail::_connectWifi() const{
  if(_connectToWifi(_ssid_wifi,_pass_wifi)){
    return 12;
  }
  else{
    return 0;
  }
}

int FrogSendGmail::connectWifiAndSendSMTPGmail(const char *subject, 
                                               const char *messageBody,
                                               const char *receiver, 
                                               const char *receiverName){  // last argument is optional
  _receiverEmail  = receiver;
  _receiverName   = receiverName;
  _messageBody    = messageBody;
  
  const char *SubjectHeader = "Subject: ";
  char SubjectBuff[strlen(SubjectHeader)+strlen(subject)+1];
  sprintf(SubjectBuff,"%s%s",SubjectHeader,subject);
  _subject     = SubjectBuff;

  _client = new WiFiSSLClient(); 

  int errorIndex = 0,
      i = 0;
  while(!errorIndex && i<_nbSendFuncs){
    errorIndex = (this->*_sendFuncVec[i++])();
  }
  return errorIndex;
}
int FrogSendGmail::_connectServer() const{              // funcIndex = 0, errorMsgIndex =  1 and 2
  Serial.println("\nConnecting to server: " + String(_server) +":" +String(_port));

  if (_client->connectSSL(_server, _port)==1){ 
    Serial.println("Connected to server");
    if (_response(_client)){
      String s = _server + String(" port:")+ String(_port);
      Serial.print("no reply on connect to ");
      Serial.println(s);
      return 2;
    }
  }
  else{
    return 1;
  }
  return 0;
}
int FrogSendGmail::_send_EHLO() const{                  // funcIndex = 1, errorMsgIndex =  3
  char ehlobuff[100];
  sprintf(ehlobuff,"EHLO %s",_sendDomain);
  Serial.println("Sending Extended Hello: <start>" + String(ehlobuff) + "<end>"); //EHLO xxx.org<end>");
  _client->println(ehlobuff); // "EHLO xxx.org");
  if (_response(_client)){
    Serial.println("no reply" + String(ehlobuff)); // EHLO xxx.org");
    return 3;
  }
  return 0;
}
int FrogSendGmail::_send_AUTH_LOGIN() const{            // funcIndex = 2, errorMsgIndex =  4
  Serial.println("Sending auth login: <start>AUTH LOGIN<end>");
  _client->println(F("AUTH LOGIN"));
  if (_response(_client)){
    Serial.println("no reply AUTH LOGIN");
    return 4;
  }
  return 0;
}
int FrogSendGmail::_send_GMail_User_Account() const{    // funcIndex = 3, errorMsgIndex =  5
  Serial.println("Sending account: <start>" +String(_encodedAccount) + "<end>");
  _client->println(F(_encodedAccount)); 
  if (_response(_client)){
    Serial.println("no reply to Sending User");
    return 5;
  }
  return 0;  
}
int FrogSendGmail::_send_Gmail_Password() const{        // funcIndex = 4, errorMsgIndex =  6
  Serial.println("Sending Password: <start>" +String(_encodedPass) + "<end>");  
  _client->println(F(_encodedPass)); 
  if (_response(_client)){
    Serial.println("no reply Sending Password");
    return 6;
  }
  return 0;
}
int FrogSendGmail::_send_From() const{                  // funcIndex = 5, errorMsgIndex =  7
  char frombuff[100];
  sprintf(frombuff,"MAIL FROM: <%s>",_gAcc.c_str());

  Serial.println("Sending From: <start>" + String(frombuff) + "<end>"); //MAIL FROM: <xyz@gmail.com><end>");
  // your email address (sender) - MUST include angle brackets
  _client->println(F(frombuff)); //"MAIL FROM: <xyz@gmail.com>"));
  if (_response(_client)){
    Serial.println("no reply Sending From");
    return 7;
  }
  return 0;
}
int FrogSendGmail::_send_To() const{                    // funcIndex = 6, errorMsgIndex =  8
  char tobuff[100];
  sprintf(tobuff,"RCPT To: <%s>",_receiverEmail); //_recvMail);
  
  // change to recipient address - MUST include angle brackets
  Serial.println("Sending To: <start>" + String(tobuff) + "<end>"); //RCPT To: <abc@gmail.com><end>");
  _client->println(F(tobuff));
  if (_response(_client)){
    Serial.println("no reply Sending To");
    return 8;
  }
  return 0;
}
int FrogSendGmail::_send_DATA() const{                  // funcIndex = 7, errorMsgIndex =  9
  Serial.println("Sending DATA: <start>DATA<end>");
  _client->println(F("DATA"));
  if (_response(_client)){
    Serial.println("no reply Sending DATA");
    return 9;
  }
  return 0;
}
int FrogSendGmail::_send_Headers_And_Body() const{      // funcIndex = 8, errorMsgIndex = 10
  Serial.println("Sending email");
  char tobuffA[100];
  if (_receiverName){
    sprintf(tobuffA,"To: %s <%s>",_receiverName,_receiverEmail);
  }
  else{
    sprintf(tobuffA,"To: <%s>",_receiverEmail);
  }
  // recipient address (include optional display name if you want)
  
  char frombuffA[100];
  sprintf(frombuffA,"From: %s <%s>",_gName.c_str(),_gAcc.c_str());

  // send the to address, the from address, subject and the message
  _client->println(F(tobuffA));
  _client->println(F(frombuffA));
  _client->println(F(_subject));
  _client->println(F(_messageBody));

  //const char outgoing[] = "Alot of things were fixed up in this code, but will it ever be finished?\n";
  //_client->println(F("Subject: Good 2 Go! (from CODE)"));
  //client.println(F(outgoing));
 
  // IMPORTANT you must send a complete line containing just a "." to end the conversation
  // So the PREVIOUS line to this one must be a println not just a print
  _client->println(F("."));
  if (_response(_client)){
    Serial.println("no reply Sending '.'");
    return 10;
  }
  return 0;
}
int FrogSendGmail::_send_QUIT() const{                  // funcIndex = 9, errorMsgIndex = 11
  Serial.println(F("Sending QUIT"));
  _client->println(F("QUIT"));
  if (_response(_client)){
    Serial.println("no reply Sending QUIT");
    return 11;
  }
  _client->stop();
  return 0;
}
