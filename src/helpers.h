#ifndef HELPERS_H
#define HELPERS_H

#include <Arduino.h>
#include <SPI.h>
#include <WiFiNINA.h>
#include "GF_Base64.h"

extern int _connectToWifi(const char *ssid, const char *pass);
extern void _printWiFiStatus();
extern byte _response(WiFiSSLClient *client);
extern void _encode64(String InputString, char *res);   
  /* res must point to a char[] of the proper length
   *  usage:
   *    int encodedLength = Base64.encodedLength(InputString.length());
   *    char encodedString[encodedLength+1];
   *    encode64(InputString, encodedString);
   *    res[encodedLength] = '\0';
   */

#endif
