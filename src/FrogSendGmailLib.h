#ifndef FROG_SEND_GMAIL_LIB_H
#define FROG_SEND_GMAIL_LIB_H

/*  Usage:
  char *toAddress   = "FrankJomes@frank.com",
       *toName      = "Frank",        // the name is optional
       *subjectText = "I'm tired of being lumberjack!", 
       *msgTest     = "But, I'm ok.";  
     
  FrogSendGmail *fsg = new FrogSendGmail();
  int err0 = fsg->connectWifiAndSendSMTPGmail(subjectText, msgTest,toAddress, toName);  // toName is an optional argument
  Serial.println(String(FrogSendGmail::errorMessageVec[err0]));
 */

// do not modify these next defines!
#define GMAIL_SMTP_SERVER               ("smtp.gmail.com")
#define GMAIL_SERVER_PORT               (465)

#include <Arduino.h>
#include <SPI.h>
#include <WiFiNINA.h>
#include <stdio.h>

#include "helpers.h"

class FrogSendGmail;

typedef int (FrogSendGmail::*sendFuncPtr)()const;

class FrogSendGmail{
  public:
    static const String errorMessageVec[];
  protected:
    const char * const _ssid_wifi;     // your network SSID (name)
    const char * const _pass_wifi;     // your network password (use for WPA)

    const char *const _sendDomain;    // sender's domain

    const String _gAcc,       // "something@gmail.com"
                 _gPass,      // "gmail_password"
                 _gName;      // "sender's name"

    const int _port;              //465 //587 for TLS does not work
    const char * const _server;    // name address for Gmail SMTP (using DNS)

    // these get assigned by user in method calls
    char *_encodedAccount,
         *_encodedPass;
    const char *_receiverEmail,     // receiver's email address
               *_receiverName,      // optional
               *_subject,
               *_messageBody;
    
    WiFiSSLClient *_client;

    int _b64Encode(String inputString,char *outputCharVec) const; // outputCharVec must be of lenght inputSTring.length()+1 !!

    // return 0 if all ok, error code otherwise
    int _connectWifi() const;                // funcIndex = 0, errorMsgIndex =  1 and 2
    int _connectServer() const;              // funcIndex = 1, errorMsgIndex =  1 and 2
    int _send_EHLO() const;                  // funcIndex = 2, errorMsgIndex =  3
    int _send_AUTH_LOGIN() const;            // funcIndex = 3, errorMsgIndex =  4
    int _send_GMail_User_Account() const;    // funcIndex = 4, errorMsgIndex =  5
    int _send_Gmail_Password() const;        // funcIndex = 5, errorMsgIndex =  6
    int _send_From() const;                  // funcIndex = 6, errorMsgIndex =  7
    int _send_To() const;                    // funcIndex = 7, errorMsgIndex =  8
    int _send_DATA() const;                  // funcIndex = 8, errorMsgIndex =  9
    int _send_Headers_And_Body() const;      // funcIndex = 9, errorMsgIndex = 10
    int _send_QUIT() const;                  // funcIndex = 10, errorMsgIndex = 11
    
    static const int _nbSendFuncs = 11;
    static const sendFuncPtr _sendFuncVec[_nbSendFuncs];
    
  public:
    FrogSendGmail(const char *wifiSSID,
		  const char *wifiPasword,
		  const char *senderDomain,
		  const char *senderGmailAddress,
		  const char *senderGmailAppPAssword,
		  const char *senderName);
    // return 0 if all ok, error code otherwise
    int connectWifiAndSendSMTPGmail(const char *subject,
				    const char *messageBody,
				    const char *receiver,
				    const char *receiverName= NULL);
    
};

#endif
